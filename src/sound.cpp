// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "sound.hpp"

#include <iostream>
#include <string>

#include "actor_mon.hpp"
#include "actor_player.hpp"
#include "feature_rigid.hpp"
#include "flood.hpp"
#include "game_time.hpp"
#include "map.hpp"
#include "map_parsing.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static const int s_snd_dist_normal = g_fov_radi_int;

static const int s_snd_dist_loud = s_snd_dist_normal * 2;

// -----------------------------------------------------------------------------
// Sound
// -----------------------------------------------------------------------------
Snd::Snd(
        const std::string& msg,
        const SfxId sfx,
        const IgnoreMsgIfOriginSeen ignore_msg_if_origin_seen,
        const P& origin,
        Actor* const actor_who_made_sound,
        const SndVol vol,
        const AlertsMon alerting_mon,
        const MorePromptOnMsg add_more_prompt_on_msg,
        std::shared_ptr<SndHeardEffect> snd_heard_effect) :

        m_msg(msg),
        m_sfx(sfx),
        m_is_msg_ignored_if_origin_seen(ignore_msg_if_origin_seen),
        m_origin(origin),
        m_actor_who_made_sound(actor_who_made_sound),
        m_vol(vol),
        m_is_alerting_mon(alerting_mon),
        m_add_more_prompt_on_msg(add_more_prompt_on_msg),
        m_snd_heard_effect(snd_heard_effect)
{

}

Snd::~Snd()
{

}

void Snd::run()
{
        snd_emit::run(*this);
}

void Snd::on_heard(Actor& actor) const
{
        if (m_snd_heard_effect)
        {
                m_snd_heard_effect->run(actor);
        }
}

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static int s_nr_snd_msg_printed_current_turn;


static int get_max_dist(const Snd& snd)
{
        return
                snd.is_loud()
                ? s_snd_dist_loud
                : s_snd_dist_normal;
}

static bool is_snd_heard_at_range(const int range, const Snd& snd)
{
        return range <= get_max_dist(snd);
}

// -----------------------------------------------------------------------------
// Sound emitting
// -----------------------------------------------------------------------------
namespace snd_emit
{

void reset_nr_snd_msg_printed_current_turn()
{
        s_nr_snd_msg_printed_current_turn = 0;
}

void run(Snd snd)
{
        ASSERT(snd.msg() != " ");

        Array2<bool> blocked(map::dims());

        map_parsers::BlocksSound()
                .run(blocked, blocked.rect());

        const P& origin = snd.origin();

        // Never block the origin - we want to be able to run the sound from
        // e.g. a closing door, after it was closed (and we don't want this to
        // depend on the floodfill algorithm, so we explicitly set the origin to
        // free here)
        blocked.at(origin.x, origin.y) = false;

        const int snd_max_dist = get_max_dist(snd);

        const auto flood = floodfill(
                origin,
                blocked,
                snd_max_dist,
                P(-1, -1),
                true);

        flood.at(origin.x, origin.y) = 0;

        for (Actor* actor : game_time::g_actors)
        {
                const int flood_val_at_actor = flood.at(actor->m_pos);

                const P& actor_pos = actor->m_pos;

                if (((flood_val_at_actor == 0) && (actor_pos != origin)) ||
                    !is_snd_heard_at_range(flood_val_at_actor, snd))
                {
                        continue;
                }

                const bool is_origin_seen_by_player =
                        map::g_cells.at(origin).is_seen_by_player;

                if (actor->is_player())
                {
                        if (is_origin_seen_by_player &&
                            snd.is_msg_ignored_if_origin_seen())
                        {
                                snd.clear_msg();
                        }

                        if (!snd.msg().empty())
                        {
                                // Add a direction to the message (i.e. "(NW)")
                                if (actor_pos != origin)
                                {
                                        const std::string dir_str =
                                                dir_utils::compass_dir_name(
                                                        actor_pos,
                                                        origin);

                                        snd.add_string("(" + dir_str + ")");
                                }
                        }

                        const int pct_dist =
                                (flood_val_at_actor * 100) / snd_max_dist;

                        const P offset = (origin - actor_pos).signs();

                        const Dir dir_to_origin = dir_utils::dir(offset);

                        map::g_player->hear_sound(
                                snd,
                                is_origin_seen_by_player,
                                dir_to_origin, pct_dist);
                }
                else // Not player
                {
                        Mon* const mon = static_cast<Mon*>(actor);

                        mon->hear_sound(snd);
                }
        }
}

} // snd_emit
