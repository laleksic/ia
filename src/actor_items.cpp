// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "actor_items.hpp"

#include <vector>

#include "actor.hpp"
#include "actor_mon.hpp"
#include "actor_player.hpp"
#include "item_data.hpp"
#include "item_factory.hpp"
#include "item_rod.hpp"
#include "map.hpp"
#include "player_spells.hpp"
#include "spells.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static void learn_spell_player(const SpellId spell)
{
        player_spells::learn_spell(spell, Verbosity::silent);

        // Also identify and "find" the corresponding scroll
        for (auto& d : item_data::g_data)
        {
                if (d.spell_cast_from_scroll == spell)
                {
                        std::unique_ptr<Item> temp_scroll(
                                item_factory::make(d.id));

                        temp_scroll->identify(Verbosity::silent);

                        game::incr_player_xp(
                                temp_scroll->data().xp_on_found,
                                Verbosity::silent);

                        temp_scroll->data().is_found = true;
                }
        }
}

static void make_for_player_occultist_common()
{
        auto& inv = map::g_player->m_inv;

        inv.put_in_slot(
                SlotId::wpn,
                item_factory::make(ItemId::hatchet),
                Verbosity::silent);

        inv.put_in_slot(
                SlotId::wpn_alt,
                item_factory::make(ItemId::pistol),
                Verbosity::silent);

        inv.put_in_backpack(item_factory::make(ItemId::pistol_mag));

        inv.put_in_slot(
                SlotId::body,
                item_factory::make(ItemId::armor_leather_jacket),
                Verbosity::silent);

        Item* spirit_pot = item_factory::make(ItemId::potion_spirit);

        spirit_pot->identify(Verbosity::silent);

        game::incr_player_xp(
                spirit_pot->data().xp_on_found,
                Verbosity::silent);

        spirit_pot->data().is_found = true;

        inv.put_in_backpack(spirit_pot);

        map::g_player->set_unarmed_wpn(
                static_cast<Wpn*>(
                        item_factory::make(ItemId::player_punch)));

        inv.put_in_backpack(item_factory::make(ItemId::dynamite, 1));
        inv.put_in_backpack(item_factory::make(ItemId::molotov, 1));
        inv.put_in_backpack(item_factory::make(ItemId::medical_bag));
        inv.put_in_backpack(item_factory::make(ItemId::lantern));
}

static void make_for_player_occultist_transmut()
{
        learn_spell_player(SpellId::slow_time);
        learn_spell_player(SpellId::transmut);
}

static void make_for_player_occultist_clairv()
{
        learn_spell_player(SpellId::searching);
        learn_spell_player(SpellId::identify);
}

static void make_for_player_occultist_ench()
{
        learn_spell_player(SpellId::terrify);
        learn_spell_player(SpellId::heal);
}

static void make_for_player_occultist_invoc()
{
        learn_spell_player(SpellId::darkbolt);
        learn_spell_player(SpellId::aura_of_decay);
}

// static void make_for_player_occultist_summon()
// {
//         learn_spell_player(SpellId::summon);
// }

static void make_for_player_rogue()
{
        auto& inv = map::g_player->m_inv;

        auto* const dagger = item_factory::make(ItemId::dagger);

        static_cast<Wpn*>(dagger)->m_melee_base_dmg.plus = 1;

        inv.put_in_slot(SlotId::wpn,
                        dagger,
                        Verbosity::silent);

        inv.put_in_slot(SlotId::wpn_alt,
                        item_factory::make(ItemId::pistol),
                        Verbosity::silent);

        for (int i = 0; i < 2; ++i)
        {
                inv.put_in_backpack(item_factory::make(ItemId::pistol_mag));
        }

        inv.put_in_slot(SlotId::body,
                        item_factory::make(ItemId::armor_leather_jacket),
                        Verbosity::silent);

        inv.put_in_backpack(item_factory::make(ItemId::iron_spike, 12));

        Item* rod_cloud_minds = item_factory::make(ItemId::rod_cloud_minds);

        rod_cloud_minds->identify(Verbosity::silent);

        game::incr_player_xp(rod_cloud_minds->data().xp_on_found,
                             Verbosity::silent);

        rod_cloud_minds->data().is_found = true;

        inv.put_in_backpack(rod_cloud_minds);

        map::g_player->set_unarmed_wpn(
                static_cast<Wpn*>(
                        item_factory::make(ItemId::player_punch)));

        inv.put_in_backpack(item_factory::make(ItemId::dynamite, 2));
        inv.put_in_backpack(item_factory::make(ItemId::molotov, 2));
        inv.put_in_backpack(item_factory::make(ItemId::medical_bag));
        inv.put_in_backpack(item_factory::make(ItemId::lantern));

        Item* const throwing_knives = item_factory::make(ItemId::thr_knife, 12);

        inv.put_in_backpack(throwing_knives);

        map::g_player->m_last_thrown_item = throwing_knives;
}

static void make_for_player_war_vet()
{
        auto& inv = map::g_player->m_inv;

        inv.put_in_slot(
                SlotId::wpn,
                item_factory::make(ItemId::machete),
                Verbosity::silent);

        for (int i = 0; i < 3; ++i)
        {
                inv.put_in_backpack(item_factory::make(ItemId::pistol_mag));
        }

        inv.put_in_slot(
                SlotId::wpn_alt,
                item_factory::make(ItemId::pistol),
                Verbosity::silent);

        inv.put_in_slot(
                SlotId::body,
                item_factory::make(ItemId::armor_flak_jacket),
                Verbosity::silent);

        map::g_player->set_unarmed_wpn(
                static_cast<Wpn*>(
                        item_factory::make(ItemId::player_punch)));

        inv.put_in_backpack(item_factory::make(ItemId::dynamite, 2));
        inv.put_in_backpack(item_factory::make(ItemId::molotov, 2));
        inv.put_in_backpack(item_factory::make(ItemId::smoke_grenade, 4));
        inv.put_in_backpack(item_factory::make(ItemId::flare, 2));
        inv.put_in_backpack(item_factory::make(ItemId::medical_bag));
        inv.put_in_backpack(item_factory::make(ItemId::lantern));
        inv.put_in_backpack(item_factory::make(ItemId::gas_mask));

        Item* const throwing_knives = item_factory::make(ItemId::thr_knife, 6);

        inv.put_in_backpack(throwing_knives);

        map::g_player->m_last_thrown_item = throwing_knives;
}

static void make_for_player_ghoul()
{
        map::g_player->set_unarmed_wpn(
                static_cast<Wpn*>(
                        item_factory::make(ItemId::player_ghoul_claw)));
}

static void make_for_player()
{
        switch (player_bon::bg())
        {
        case Bg::occultist:
                make_for_player_occultist_common();

                switch (player_bon::occultist_domain())
                {
                case OccultistDomain::clairvoyant:
                        make_for_player_occultist_clairv();
                        break;

                case OccultistDomain::enchanter:
                        make_for_player_occultist_ench();
                        break;

                case OccultistDomain::invoker:
                        make_for_player_occultist_invoc();
                        break;

                        // case OccultistDomain::summoner:
                        //         make_for_player_occultist_summoner();
                        //         break;

                case OccultistDomain::transmuter:
                        make_for_player_occultist_transmut();
                        break;

                case OccultistDomain::END:
                        ASSERT(false);
                        break;

                } // Occultist domain switch
                break;

        case Bg::rogue:
                make_for_player_rogue();
                break;

        case Bg::war_vet:
                make_for_player_war_vet();
                break;

        case Bg::ghoul:
                make_for_player_ghoul();
                break;

        case Bg::END:
                break;
        } // Background switch
}

static void make_random_item_to_backpack(Actor& actor,
                                         std::vector<ItemId>& item_id_bucket)
{
        if (!item_id_bucket.empty())
        {
                const ItemId item_id = rnd::element(item_id_bucket);

                Item* item = item_factory::make(item_id);

                actor.m_inv.put_in_backpack(item);
        }
}

static void make_item_set_minor_treasure(Actor& actor)
{
        std::vector<ItemId> item_bucket;

        for (int i = 0; i < (int)ItemId::END; ++i)
        {
                const ItemData& d = item_data::g_data[i];

                if (d.value == ItemValue::minor_treasure)
                {
                        item_bucket.push_back((ItemId)i);
                }
        }

        make_random_item_to_backpack(actor, item_bucket);
}

static void make_item_set_rare_treasure(Actor& actor)
{
        std::vector<ItemId> item_bucket;

        for (int i = 0; i < (int)ItemId::END; ++i)
        {
                const ItemData& d = item_data::g_data[i];

                if (d.value == ItemValue::rare_treasure)
                {
                        item_bucket.push_back((ItemId)i);
                }
        }

        make_random_item_to_backpack(actor, item_bucket);
}

static void make_item_set_supreme_treasure(Actor& actor)
{
        std::vector<ItemId> item_bucket;

        for (int i = 0; i < (int)ItemId::END; ++i)
        {
                const ItemData& d = item_data::g_data[i];

                if (d.value == ItemValue::supreme_treasure)
                {
                        item_bucket.push_back((ItemId)i);
                }
        }

        make_random_item_to_backpack(actor, item_bucket);
}

static void make_item_set_firearm(Actor& actor)
{
        Inventory& inv = actor.m_inv;

        // If we are on an early dungeon level, lean heavily towards pistols
        const bool is_low_dlvl = map::g_dlvl < 4;

        std::vector<int> weights = {
                (is_low_dlvl ? 20 : 6), // Pistol
                3,                      // Pump shotgun
                3,                      // Sawed-off shotgun
                1                       // Machine Gun
        };

        const int choice = rnd::weighted_choice(weights);

        switch (choice)
        {
        case 0:
        {
                // Pistol
                Item* item = item_factory::make(ItemId::pistol);

                Wpn* wpn = static_cast<Wpn*>(item);

                const int ammo_cap = wpn->data().ranged.max_ammo;

                wpn->m_ammo_loaded = rnd::range(ammo_cap / 2, ammo_cap);

                inv.put_in_slot(SlotId::wpn, item, Verbosity::silent);

                if (rnd::coin_toss())
                {
                        inv.put_in_backpack(
                                item_factory::make(ItemId::pistol_mag));
                }
        }
        break;

        case 1:
        {
                // Pump shotgun
                Item* item = item_factory::make(ItemId::pump_shotgun);

                Wpn* wpn = static_cast<Wpn*>(item);

                const int ammo_cap = wpn->data().ranged.max_ammo;

                wpn->m_ammo_loaded = rnd::range(ammo_cap / 2, ammo_cap);

                inv.put_in_slot(SlotId::wpn, item, Verbosity::silent);

                item = item_factory::make(ItemId::shotgun_shell);

                item->m_nr_items = rnd::range(1, 6);

                inv.put_in_backpack(item);
        }
        break;

        case 2:
        {
                // Sawed-off shotgun
                inv.put_in_slot(SlotId::wpn,
                                item_factory::make(ItemId::sawed_off),
                                Verbosity::silent);

                Item* item = item_factory::make(ItemId::shotgun_shell);

                item->m_nr_items = rnd::range(1, 6);

                inv.put_in_backpack(item);
        }
        break;

        case 3:
        {
                // Tommy Gun

                // Number of bullets loaded needs to be a multiple of the number
                // of projectiles fired in each burst
                Item* item = item_factory::make(ItemId::machine_gun);

                Wpn* const wpn = static_cast<Wpn*>(item);

                const int cap_scaled =
                        wpn->data().ranged.max_ammo /
                        g_nr_mg_projectiles;

                const int min_scaled = cap_scaled / 2;

                wpn->m_ammo_loaded =
                        rnd::range(min_scaled, cap_scaled) *
                        g_nr_mg_projectiles;

                inv.put_in_slot(SlotId::wpn, item, Verbosity::silent);
        }
        break;
        }
}

static void make_item_set_spike_gun(Actor& actor)
{
        Inventory& inv = actor.m_inv;

        {
                Item* item = item_factory::make(ItemId::spike_gun);

                Wpn* wpn = static_cast<Wpn*>(item);

                const int ammo_cap = wpn->data().ranged.max_ammo;

                wpn->m_ammo_loaded = rnd::range(ammo_cap / 2, ammo_cap);

                inv.put_in_slot(SlotId::wpn, item, Verbosity::silent);
        }

        {
                Item* item = item_factory::make(ItemId::iron_spike);

                item->m_nr_items = rnd::range(1, 6);

                inv.put_in_backpack(item);
        }
}

static void make_item_set_zealot_spiked_mace(Actor& actor)
{
        Item* item = item_factory::make(ItemId::spiked_mace);

        item->m_melee_base_dmg.plus = 0;

        actor.m_inv.put_in_slot(SlotId::wpn, item, Verbosity::silent);
}

static void make_item_set_priest_dagger(Actor& actor)
{
        Item* item = item_factory::make(ItemId::dagger);

        const std::vector<int> weights = {
                6,
                3,
                1
        };

        item->m_melee_base_dmg.plus = rnd::weighted_choice(weights) + 1;

        actor.m_inv.put_in_slot(SlotId::wpn, item, Verbosity::silent);
}

static void make_item_set_mi_go_gun(Actor& actor)
{
        actor.m_inv.put_in_slot(
                SlotId::wpn,
                item_factory::make(ItemId::mi_go_gun),
                Verbosity::silent);
}

static void make_item_set_mi_go_armor(Actor& actor)
{
        actor.m_inv.put_in_slot(
                SlotId::body,
                item_factory::make(ItemId::armor_mi_go),
                Verbosity::silent);
}

static void make_item_set_high_priest_guard_war_vet(Actor& actor)
{
        actor.m_inv.put_in_slot(
                SlotId::wpn,
                item_factory::make(ItemId::machine_gun),
                Verbosity::silent);
}

static void make_item_set_high_priest_guard_rogue(Actor& actor)
{
        Item* const item = item_factory::make(ItemId::machete);

        item->m_melee_base_dmg.plus = 1;

        actor.m_inv.put_in_slot(
                SlotId::wpn,
                item,
                Verbosity::silent);
}

static void make_monster_item_sets(Actor& actor)
{
        for (const ActorItemSetData& item_set : actor.m_data->item_sets)
        {
                if (!rnd::percent(item_set.pct_chance_to_spawn))
                {
                        continue;
                }

                const int nr = item_set.nr_spawned_range.roll();

                for (int i = 0; i < nr; ++i)
                {
                        switch (item_set.item_set_id)
                        {
                        case ItemSetId::minor_treasure:
                                make_item_set_minor_treasure(actor);
                                break;

                        case ItemSetId::rare_treasure:
                                make_item_set_rare_treasure(actor);
                                break;

                        case ItemSetId::supreme_treasure:
                                make_item_set_supreme_treasure(actor);
                                break;

                        case ItemSetId::firearm:
                                make_item_set_firearm(actor);
                                break;

                        case ItemSetId::spike_gun:
                                make_item_set_spike_gun(actor);
                                break;

                        case ItemSetId::zealot_spiked_mace:
                                make_item_set_zealot_spiked_mace(actor);
                                break;

                        case ItemSetId::priest_dagger:
                                make_item_set_priest_dagger(actor);
                                break;

                        case ItemSetId::mi_go_gun:
                                make_item_set_mi_go_gun(actor);
                                break;

                        case ItemSetId::mi_go_armor:
                                make_item_set_mi_go_armor(actor);
                                break;

                        case ItemSetId::high_priest_guard_war_vet:
                                make_item_set_high_priest_guard_war_vet(actor);
                                break;

                        case ItemSetId::high_priest_guard_rogue:
                                make_item_set_high_priest_guard_rogue(actor);
                                break;
                        }
                }
        }
}

static void make_monster_intr_attacks(Actor& actor)
{
        for (auto& intr_attack : actor.m_data->intr_attacks)
        {
                auto* item = item_factory::make(intr_attack->item_id);

                // Override damage with the damage in the intrinsic attack data
                // (we always override both melee and ranged damage - this
                // doesn't matter, since only one damage type will be used and
                // the other will have no effect)
                const auto dice = Dice(1, intr_attack->dmg);

                item->m_melee_base_dmg = dice;
                item->m_ranged_base_dmg = dice;

                actor.m_inv.put_in_intrinsics(item);
        }
}

static void make_monster_spells(Actor& actor)
{
        ASSERT(!actor.is_player());

        if (actor.is_player())
        {
                return;
        }

        Mon* const mon = static_cast<Mon*>(&actor);

        for (auto& spell_data : actor.m_data->spells)
        {
                if (!rnd::percent(spell_data.pct_chance_to_know))
                {
                        continue;
                }

                Spell* const spell =
                        spell_factory::make_spell_from_id(
                                spell_data.spell_id);

                mon->add_spell(spell_data.spell_skill, spell);
        }
}

static void make_for_monster(Actor& actor)
{
        make_monster_item_sets(actor);

        make_monster_intr_attacks(actor);

        make_monster_spells(actor);
}

// -----------------------------------------------------------------------------
// actor_items
// -----------------------------------------------------------------------------
namespace actor_items
{

void make_for_actor(Actor& actor)
{
        if (actor.is_player())
        {
                make_for_player();
        }
        else // Not player
        {
                make_for_monster(actor);
        }
}

} // actor_items
