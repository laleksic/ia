// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef COMMON_MESSAGES_HPP
#define COMMON_MESSAGES_HPP

#include <string>

namespace common_text
{

extern const std::string g_screen_exit_hint;
extern const std::string g_scrollable_info_screen_hint;
extern const std::string g_next_page_up_hint;
extern const std::string g_next_page_down_hint;
extern const std::string g_cancel_hint;
extern const std::string g_confirm_hint;
extern const std::string g_any_key_hint;
extern const std::string g_yes_or_no_hint;
extern const std::string g_disarm_no_trap;
extern const std::string g_mon_prevent_cmd;
extern const std::string g_fire_prevent_cmd;
extern const std::string g_mon_disappear;

} // common_text

#endif // COMMON_MESSAGES_HPP
