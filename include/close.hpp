// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef CLOSE_HPP
#define CLOSE_HPP

namespace close_door
{

void player_try_close_or_jam();

} // close_door

#endif // CLOSE_HPP
